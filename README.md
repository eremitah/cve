Codes and proof-of-concepts for:

- cve.mitre.org

- nvd.nist.gov

- seclists.org/fulldisclosure

- seclists.org/oss-sec